# BackupOrNot

This is a simple script to decide if do a backup or not based on network conditions.  
You can decide to do backup on a networks whitelist or if the metered connection is activated (with GNOME nm-connection-editor metered connection).  
Why? I decided to write this cause an automated backup with a backup software can't be possible in some networks (eg. public network with slow connections, backup host it's not on the network, not trustworth network) and systemd can't manage these situations. However this script has to work with a scheduled event made on systemd.

Probably not the best script, but once it's all setted up (systemd, restic, script) should work with no problem.

## Requirements

 - ifconfig (for analysis of connections status)
 - nmcli/network-manager (for analysis of background data restriction) 
 - notify-send (for notifications)
 - A command line backup software (Restic, Borg, etc.)

 ## ToDo
 
 - Check a local host 
 - If whitelist is empty skip the check an go directly on background data check
 - If backup fail, try again after a small amount of time (eg. if backup it's daily and it fails, try again 3 hours later)

## My Config

### Systemd 

In ``` ~/.config/ ``` put the configuration file *nameFile.conf*  

    BACKUP_PATHS="/folder/toBackup"  
    BACKUP_EXCLUDES="" 
    WHITELIST="/folder/whitelist" 
    RETENTION_DAYS=7  
    RETENTION_WEEKS=4  
    RETENTION_MONTHS=6  
    RETENTION_YEARS=3  
    B2_ACCOUNT_ID=XXXXXXXXXXXXXXXXXXXXXXXXX  
    B2_ACCOUNT_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  
    RESTIC_REPOSITORY=/folder/repo  
    RESTIC_PASSWORD=passwordToSet 


In ``` ~/.config/systemd/user ``` put the service file *nameFile.service*  

    [Unit]  
    Description=Restic backup service  
    Wants=network-online.target  
    After=network-online.target  
    [Service]  
    Type=daily  
    ExecStart=/script/folder/BackupOrNot.sh  
    EnvironmentFile=%h/.config/restic-backup.conf  

In ``` ~/.config/systemd/user ``` put the service file *nameFile.timer*

    [Unit]  
    Description=Backup with restic daily  
    [Timer]  
    OnCalendar=daily  
    Persistent=true  
    [Install]  
    WantedBy=timers.target  

### Backup SW

At the moment I use Restic CLI, but this script should work with no problem also with other softwares like borg or duplicity