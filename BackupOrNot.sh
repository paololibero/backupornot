#!/bin/bash

# A script to setup backup on certain networks conditions

# Backup script. Set environmental variables on .conf file
executeBackup() {
    restic backup --verbose --one-file-system --tag systemd.timer $BACKUP_EXCLUDES $BACKUP_PATHS
    restic forget --verbose --tag systemd.timer --group-by "paths,tags" --keep-daily $RETENTION_DAYS --keep-weekly $RETENTION_WEEKS --keep-monthly $RETENTION_MONTHS --keep-yearly $RETENTION_YEARS
}

# Check on wired connection
wiredConnection=`ifconfig enp2s0 | grep RUNNING`

if [[ $wiredConnection == *"RUNNING"* ]]; then
    wiredConnectionName=`nmcli device show enp2s0 | grep "GENERAL.CONNECTION" | cut -d ":" -f2 | xargs`
    wiredConnectionMetered=`nmcli -f connection.metered connection show "$wiredConnectionName" | head -n1 | cut -d ":" -f2 | xargs`

    if [[ $wiredConnectionMetered != *"yes"* ]]; then
        executeBackup
        echo "Backup done! (on ETH)";
        notify-send "Backup done!" "Backup done on ETH network interface" --icon=dialog-information
        exit 0;
    else 
        if [[ $wiredConnectionMetered == *"yes"* ]]; then
            echo "Restrict background data usage (on ETH)"
            notify-send "Backup error" "Restrict background data usage on ETH network interface. Can't backup" --icon=dialog-error
            exit 1;
        fi
    fi
fi

# Check on wireless connection
wirelessConnection=`ifconfig wlp3s0 | grep RUNNING`

# Network SSID whitelist
declare -a allowedWirelessNetworks
file="$WHITELIST"

while IFS= read line
do
    allowedWirelessNetworks+=("$line")
done < "$file"

if [[ $wirelessConnection == *"RUNNING"* ]]; then
    wirelessConnectionName=`nmcli device show wlp3s0 | grep "GENERAL.CONNECTION" | cut -d ":" -f2 | xargs`
    wirelessConnectionMetered=`nmcli -f connection.metered connection show "$wirelessConnectionName" | head -n1 | cut -d ":" -f2 | xargs`

    for val in "${allowedWirelessNetworks[@]}";
        do
            if [[ $wirelessConnectionName == $val && $wirelessConnectionMetered != *"yes"* ]]; then
                executeBackup
                echo "Backup done! (on WLAN)";
                notify-send "Backup done!" "Backup done on WLAN network interface" --icon=dialog-information
                exit 0;
            else
                if [[ $wirelessConnectionName == $val && $wirelessConnectionMetered == *"yes"* ]]; then
                    echo "Restrict background data usage (on WLAN)"
                    notify-send "Backup error" "Restrict background data usage on WLAN network interface. Can't backup" --icon=dialog-error
                    exit 1;    
                fi
            fi
        done
    echo "Network not in whitelist";
    notify-send "Backup error" "This network is not on whitelist. Can't backup" --icon=dialog-error
    exit 1;
fi

# Script end. Network interface not found
echo "Not connected"
notify-send "Backup error" "Not connected. Can't backup" --icon=dialog-error
exit 1;